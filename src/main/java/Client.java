import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 * Connects to the server, then starts receiving messages. Creates
 * a MicThread that sends microphone data to the server, and creates an instance
 * of AudioThread for each user.
 * @author dosse
 */
public class Client extends Thread {


    private ArrayList<AudioChannel> chs = new ArrayList<AudioChannel>();
    Vertx vertx = Vertx.vertx();
    HttpClient client = vertx.createHttpClient();



    public static void main(String[] args) throws IOException {


        Runnable target;
        Thread Client  = new Client();
        Client.start();


    }

    public Client() throws UnknownHostException, IOException {


    }

    @Override
    public void run() {
        try {

            try {
                sleep(100); //wait for the GUI microphone test to release the microphone

            } catch (Exception e) { //error acquiring microphone. causes: no microphone or microphone busy
                System.out.println("mic unavailable " + e);
            }

            client.websocket(8080, "192.168.1.3", "/chat/arduino", websocket ->{


                websocket.handler(event -> {


                    try {
                        ObjectMapper m = new ObjectMapper();
                        String json =  event.toString();
                        m.getTypeFactory();
                        m.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        //lastPacketTime = System.nanoTime();
                        Message in = m.readValue(json, Message.class);


                        AudioChannel sendTo = null;
                        for (AudioChannel ch : chs) {
                            if (ch.getChId() == in.getChId()) {
                                sendTo = ch;
                            }
                        }
                        if (sendTo != null) {
                            sendTo.addToQueue(in);
                        } else { //new AudioChannel is needed
                            AudioChannel ch = new AudioChannel(in.getChId());
                            ch.addToQueue(in);
                            ch.start();
                            chs.add(ch);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });


            });

        } catch (Exception e) { //connection error
            System.out.println("client err " + e.toString());
        }
    }
}
 
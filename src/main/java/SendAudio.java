import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;

import javax.sound.sampled.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class SendAudio extends Thread {

    public static double amplification = 1.0;

    public String server;
    public int port;
    private TargetDataLine mic;

        public SendAudio(String server , int port) throws LineUnavailableException {

            this.server = server;
            this.port = port;

            AudioFormat af = SoundPacket.defaultFormat;
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, null);
            mic = (TargetDataLine) (AudioSystem.getLine(info));
            mic.open(af);
            mic.start();
            }




    @Override
    public void run() {
        System.out.println("init ");
        Vertx vertx = Vertx.vertx();
        HttpClient client = vertx.createHttpClient();

        client.websocket(port, server, "/chat/arduino", websocket -> {

            try {

                for(;; )
                {
                    ObjectMapper objectMapper = new ObjectMapper();



                    if (mic.available() >= SoundPacket.defaultDataLenght) { //we got enough data to send
                        byte[] buff = new byte[SoundPacket.defaultDataLenght];
                        while (mic.available() >= SoundPacket.defaultDataLenght) { //flush old data from mic to reduce lag, and read most recent data
                            mic.read(buff, 0, buff.length); //read from microphone
                        }
                        try {
                            //this part is used to decide whether to send or not the packet. if volume is too low, an empty packet will be sent and the remote client will play some comfort noise
                            long tot = 0;
                            for (int i = 0; i < buff.length; i++) {
                                buff[i] *= amplification;
                                tot += Math.abs(buff[i]);
                            }
                            tot *= 2.5;
                            tot /= buff.length;
                            //create and send packet
                            Message m = null;
                            if (tot == 0) {//send empty packet
                                m = new Message(new Header(), "maxman" , new SoundPacket(null));
                            } else { //send data
                                //compress the sound packet with GZIP
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                GZIPOutputStream go = new GZIPOutputStream(baos);
                                go.write(buff);
                                go.flush();
                                go.close();
                                baos.flush();
                                baos.close();
                                m = new Message(new Header(), "maxman", new SoundPacket(baos.toByteArray()));  //create message for server, will generate chId and timestamp from this computer's IP and this socket's port
                            }
                            String s = objectMapper.writeValueAsString(m);
                            System.out.println(s);
                            websocket.writeTextMessage(s);
                        } catch (IOException ex) { //connection error
                            stop();
                        }
                    } else {
                        sleep(10); //sleep to avoid busy wait
                    }
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        });


        }
    public static void main(String[] args) {


            Runnable target;
        try {
            Thread SendAudio  = new SendAudio("192.168.1.3" , 8080);
            SendAudio.start();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }


    }
}

public class Header {
    public String Username;
    public String RoomName;
    public int type;
    public long Length;

    public Header() {
    }

    public Header(String username, String roomName, int type, long length) {
        Username = username;
        RoomName = roomName;
        this.type = type;
        Length = length;
    }


    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getRoomName() {
        return RoomName;
    }

    public void setRoomName(String roomName) {
        RoomName = roomName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getLength() {
        return Length;
    }

    public void setLength(long length) {
        Length = length;
    }
}
